<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('common_model');

        if ($_SERVER['SERVER_NAME'] != '192.168.1.27') {
            $this->server = "http://bacancytechnology.com/btdemo/imoto/index.php/api/";
        } else {
            $this->server = "http://192.168.1.27/imoto/index.php/api/";
        }
    }

    public function index() {
        echo "Welcome To IMOTO";
    }

    public function test_signup() {
        $this->load->view('signup');
    }
	
	/* User Singnup Page  */
	
    public function signup() {

		ob_start();
		session_start();
		$this->load->library('session');
		
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);

        $success = false;
        $message = "";
        $response_data = array();

        if (!isset($agent_email) || trim($agent_email) == "" || !isset($password) || trim($password) == "" || !isset($name) || trim($name) == "" || !isset($cell_phone) || trim($cell_phone) == "" || !isset($shopper_groups) || trim($shopper_groups) == "" || !isset($office_branch) || trim($office_branch) == "" || !isset($mailing_address) || trim($mailing_address) == "" || !isset($mailing_zip) || trim($mailing_zip) == "" || !isset($city) || trim($city) == "" || !isset($state) || trim($state) == "") {
            echo $this->server . "signup?agent_email=&password=&name=&cell_phone=&shopper_groups=&office_branch=&mailing_address=&mailing_zip=&city=&state";
            echo "<br>Please pass parameters";
            exit;
        }
        $success = FALSE;
        $message = "";
        $response_data = array();
        $response_data1 = array();

        $result = $this->common_model->get_user_byemail($agent_email);
        if ($result->num_rows == 0) {
            $data['name'] = $name;
            $data['username'] = $name;
            $data['email'] = $agent_email;
            $data['password'] = encrypt($password);
            $data['usertype'] = "Registered";
            $data['registerDate'] = date('Y-m-d H:i:s');
            $data['params'] = "{}";

            $res = $this->common_model->insert("users", $data);
            $user_id = $this->db->insert_id();
            unset($data);

            $data['address_type'] = "BT";
            $data['user_id'] = $user_id;
            $data['user_email'] = $agent_email;
            $data['firstname'] = $name;
            $data['lastname'] = $name;
            $data['shopper_group_id'] = $shopper_groups;
            $data['phone'] = $cell_phone;
            $data['address'] = $mailing_address;
            $data['city'] = $city;
            $data['state_code'] = $state;
            $data['country_code'] = "USA";
            $data['zipcode'] = $mailing_zip;

            $res = $this->common_model->insert("redshop_users_info", $data);
            $item_id = $this->db->insert_id();
            unset($data);
            $this->common_model->insert("redshop_fields_data", array("fieldid" => 4, "data_txt" => $office_branch, "itemid" => $item_id, "section" => 7));

            if (isset($company)) {
                $this->common_model->insert("redshop_fields_data", array("fieldid" => 2, "data_txt" => $company, "itemid" => $item_id, "section" => 7));
            }
            if (isset($secondary_email)) {
                $this->common_model->insert("redshop_fields_data", array("fieldid" => 5, "data_txt" => $secondary_email, "itemid" => $item_id, "section" => 7));
            }
            if (isset($website)) {
                $this->common_model->insert("redshop_fields_data", array("fieldid" => 8, "data_txt" => $website, "itemid" => $item_id, "section" => 7));
            }
            if (!empty($_FILES['profile_picture']['name'])) {
                $new_name = "profile-pic-" . time();
                $path = AGENT_PROFILE_UPLOADPATH;
                $filename = $this->common_model->upload_img("profile_picture", $new_name, $path);
                $this->common_model->insert("redshop_fields_data", array("fieldid" => 9, "data_txt" => $filename, "itemid" => $item_id, "section" => 7));
            }
			
            $res = $this->common_model->insert("user_usergroup_map", array('user_id' => $user_id, 'group_id' => 2));
			
			$response_data = $this->common_model->get_user_byid($user_id)->result_array();
			
            unset($response_data[0]['fieldid']);
            unset($response_data[0]['data_txt']);

			$session_id = random_string('alnum', 26);
			$session_data['time'] = time();
			$session_data['client_id'] = '0';
			$session_data['guest'] = '0';
			$session_data['session_id'] = $session_id;
			$session_data['userid'] = $response_data[0]['id'];
			$session_data['username'] = $response_data[0]['username'];

			$this->common_model->insert("m52rw_session",$session_data);
			$response_data[0]['session_id']=$session_id;

            $success = true;
            $message = "Signup successfully.";
            $response_data1 = $response_data[0];
        } 
        else
        {
            $message = "User already exists with this E-mail id.";
        }

        $return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data1);
        echo json_encode($return_data);
    }


	
	/* Login API  */
    public function signin() {
		ob_start();
		session_start();
		$this->load->library('session');
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);

        $success = false;
        $message = "";
        $response_data = array();
        $response_data1 = array();

        if (!isset($agent_email) || trim($agent_email) == "" || !isset($password) || trim($password) == "") {
            echo $this->server . "signip?agent_email=&password=";
            echo "<br>Please pass parameters";
            exit;
        }

        $res = $this->common_model->check_user($agent_email, $password);
        if ($res == 1) 
        {
			
			$response_data = $this->common_model->get_user_data_email($agent_email)->result_array();
            $extra_detail = array();

            foreach ($response_data as $key) {
                if ($key['fieldid'] == 9) {
                    $response_data[0]['profile_picture'] = $key['data_txt'];
                }
                if ($key['fieldid'] == 8) {
                    $response_data[0]['website'] = $key['data_txt'];
                }
                if ($key['fieldid'] == 5) {
                    $response_data[0]['secondary_email'] = $key['data_txt'];
                }
                if ($key['fieldid'] == 4) {
                    $response_data[0]['office_branch'] = $key['data_txt'];
                }
                if ($key['fieldid'] == 2) {
                    $response_data[0]['company'] = $key['data_txt'];
                }
            }
            
            unset($response_data[0]['fieldid']);
            unset($response_data[0]['data_txt']);


			$check_user_session = $this->common_model->get_session_by_userid($response_data[0]['id']);
			if($check_user_session->num_rows() =='0')
			{
				$session_id = random_string('alnum', 26);
				$session_data['time'] = time();
				$session_data['client_id'] = '0';
				$session_data['guest'] = '0';
				$session_data['session_id'] = $session_id;
				$session_data['userid'] = $response_data[0]['id'];
				$session_data['username'] = $response_data[0]['username'];

				$this->common_model->insert("m52rw_session",$session_data);
				$response_data[0]['session_id']=$session_id;
			}
			else
			{
				$session_user = $check_user_session->row_array();
				$response_data[0]['session_id']	=$session_user['session_id'];
			}		
			
			

            $response_data1 = $response_data[0];
            $success = true;
            $message = "Login successfully.";
        } 
        else 
        {
            $message = "You have Enter Wrong Username or password.";
        }

        $return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data1);
        echo json_encode($return_data);
    }
    

   

    public function get_shopper_group_list() {

        $success = false;
        $user_details = array();
        $res = $this->common_model->shopper_group_list();

        if ($res->num_rows > 0) {
            $success = true;
            $message = "Shopper groups successfully retrived.";

            $user_details = $res->result_array();
        } else {
            $message = "There are no any published Shopper groups.";
        }

        $return_data = array("success" => $success, "message" => $message, 'response_data' => $user_details);
        echo json_encode($return_data);
    }

    public function get_state_list() {

        $success = false;
        $list = array();
        $res = $this->common_model->state_list();

        if ($res->num_rows > 0) {
            $success = true;
            $message = "State list successfully retrived.";
            $list = $res->result_array();
        } else {
            $message = "No any sate found.";
        }

        $return_data = array("success" => $success, "message" => $message, 'response_data' => $list);
        echo json_encode($return_data);
    }
	
	/* Forgot Password  */
	
    public function forgot_password() {

		ob_start();
		session_start();
		$this->load->library('session');
        $request_data = $_REQUEST;


        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);

        $success = false;
        $message = "";
        if (!isset($agent_email) || trim($agent_email) == "") {
            echo $this->server . "forgot_password?agent_email=";
            echo "<br>Please pass parameters";
            exit;
        }
		
        $res = $this->common_model->get_user_byemail($agent_email);
        if ($res->num_rows == 1) 
        {
            $result = $res->row_array();
            $user_id = $result['id'];
			$activation = random_string('alnum', 26);
            $res = $this->common_model->update("users", array('activation' => $activation), array('id' => $user_id));
            $reset_password_link = BASEURL."/index.php?option=com_redshop&view=password&task=changepassword&Itemid=1&token=".$activation;
            $to = $agent_email;
            $subject = "Reset Password";

            $message = "<html>
                            <head>
                                <title>Reset Password</title>
                            </head>
                            <body>
								<p>A request has been made to reset your IMOTO account password. Please click the link below to reset the password.</p>
								<p>&nbsp;</p>
								<p><a href='".$reset_password_link."' target='_blank'>Reset Password</a></p>
								<p>&nbsp;</p>
								<p>If you did not request the password change, please ignore this email.</p>
								<p>&nbsp;</p>
								<p>Thank you,</br>
								IMOTO photo</p>
                            </body>
                        </html>";
			$header = 'From:' . ADMINEMAIL . "\r\n" .
					'Reply-To: ' . ADMINEMAIL . "\r\n" .
					'X-Mailer: PHP/' . phpversion();

            // $retval = mail ($to,$subject,$message,$header);
            $retval = send_mail($to, $subject, $message);

            if ($retval == true) {
                $success = true;
                $message = "An e-mail has been sent to your e-mail address on file with instructions to reset your password.";
            } else {

                $message = "An error occurred.Please try again.";
            }
        } 
        else 
        {
            $message = "The provided email address could not be found.";
        }
        $return_data = array("success" => $success, "message" => $message);
        echo json_encode($return_data);
    }
   
	/* Get User Account Information  */
    public function get_user_account_information() {

        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);

        $success = false;
        $message = "";
        $response_data = array();
        $response_data1 = array();

        if (!isset($user_id) || trim($user_id) == "") {
            echo $this->server . "get_user_account_information?user_id=";
            echo "<br>Please pass parameters";
            exit;
        }
            $response_data = $this->common_model->get_user_data_by_user_id($user_id)->result_array();
            
            if(count($response_data)>0)
            {
                $extra_detail = array();
                foreach ($response_data as $key) {
                    if ($key['fieldid'] == 9) {
                        $response_data[0]['profile_picture'] = $key['data_txt'];
                    }
                    if ($key['fieldid'] == 8) {
                        $response_data[0]['website'] = $key['data_txt'];
                    }
                    if ($key['fieldid'] == 5) {
                        $response_data[0]['secondary_email'] = $key['data_txt'];
                    }
                    if ($key['fieldid'] == 4) {
                        $response_data[0]['office_branch'] = $key['data_txt'];
                    }
                    if ($key['fieldid'] == 2) {
                        $response_data[0]['company'] = $key['data_txt'];
                    }
                }

                unset($response_data[0]['fieldid']);
                unset($response_data[0]['fieldid']);
                unset($response_data[0]['company_name']);
                unset($response_data[0]['data_txt']);
                //array_push($response_data[0], $extra_detail);
				$response_data1 = $response_data[0];
				$success = true;
				$message = "Success";
            }
            else
            {
                $message="Not Found.";
            }

        $return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data1);
        echo json_encode($return_data);
    }
    
    /* Edit User Account Information */
	public function update_user_account_information() 
	{
		$request_data = $_REQUEST;
		extract($request_data, EXTR_SKIP);

		$success = false;
		$message = "";
		$response_data = array();

		if (!isset($user_id) || trim($user_id) == "" || !isset($cell_phone) || trim($cell_phone) == ""  || !isset($office_branch) || trim($office_branch) == "" || !isset($mailing_address) || trim($mailing_address) == "" || !isset($mailing_zip) || trim($mailing_zip) == "" || !isset($city) || trim($city) == "" || !isset($state) || trim($state) == "") {
			echo $this->server . "update_user_account_information?user_id=&agent_email=&name=&cell_phone=&office_branch=&mailing_address=&mailing_zip=&city=&state";
			echo "<br>Please pass parameters";exit;
		}
		$success = FALSE;
		$message = "";
		$response_data = array();

		$data['name'] = $name;
		$data['username'] = $name;
		$data['params'] = "{}";
		$res = $this->common_model->update("users", $data, array('id' => $user_id));
		unset($data);

		$res_info = $this->common_model->check_user_info('BT', $user_id);
		$item_id = 0;
		
		if ($res_info->num_rows == 1) 
		{
			$result_info = $res_info->row_array();
			$item_id = $result_info['users_info_id'];
			$data['firstname'] = $name;
			$data['lastname'] = $name;
			$data['shopper_group_id'] = $shopper_groups;
			$data['phone'] = $cell_phone;
			$data['address'] = $mailing_address;
			$data['city'] = $city;
			$data['state_code'] = $state;
			$data['country_code'] = "USA";
			$data['zipcode'] = $mailing_zip;

			$res = $this->common_model->update("redshop_users_info", $data, array('users_info_id' => $item_id));
			unset($data);

			if (isset($office_branch) &&  trim($office_branch)!="") {
				$this->common_model->update("redshop_fields_data", array("data_txt" => $office_branch),array("fieldid" => 4,"itemid" => $item_id, "section" => 7));
			}

			if (isset($company) && trim($company)!="") {
				$this->common_model->update("redshop_fields_data",  array("data_txt" => $company),array("fieldid" => 2,"itemid" => $item_id, "section" => 7));
			}
			if (isset($secondary_email)  && trim($secondary_email)!="") {
				$this->common_model->update("redshop_fields_data",array("data_txt" => $secondary_email), array("fieldid" => 5,"itemid" => $item_id, "section" => 7));
			}
			if (isset($website) && trim($website)!="") {
				$this->common_model->update("redshop_fields_data",array("data_txt" => $website), array("fieldid" => 8, "itemid" => $item_id, "section" => 7));
			}

			if (!empty($_FILES['profile_picture']['name'])) 
			{
				$new_name = "profile-pic-" . time();
				$path = AGENT_PROFILE_UPLOADPATH;
				$filename = $this->common_model->upload_img("profile_picture", $new_name, $path);
				$this->common_model->update("redshop_fields_data",array("data_txt" => $filename), array("fieldid" => 9, "itemid" => $item_id, "section" => 7));
			}
		}
		$success = true;
		$message = "Your account information has been successfully updated.";
		$response_data = "";

		$response_data = $this->common_model->get_user_data_by_user_id($user_id)->result_array();
		$extra_detail = array();
		foreach ($response_data as $key) 
		{
			if ($key['fieldid'] == 9) {
				$response_data[0]['profile_picture'] = $key['data_txt'];
			}
			if ($key['fieldid'] == 8) {
				$response_data[0]['website'] = $key['data_txt'];
			}
			if ($key['fieldid'] == 5) {
				$response_data[0]['secondary_email'] = $key['data_txt'];
			}
			if ($key['fieldid'] == 4) {
				$response_data[0]['office_branch'] = $key['data_txt'];
			}
			if ($key['fieldid'] == 2) {
				$response_data[0]['company'] = $key['data_txt'];
			}
		}
		unset($response_data[0]['fieldid']);
		unset($response_data[0]['data_txt']);
		//array_push($response_data[0], $extra_detail);
		$response_data1 = $response_data[0];

		$return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data1);
		echo json_encode($return_data);
	}
    
    /* Get Category List */
    public function get_category_list() {
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();
        $success = FALSE;
        $message = "";
        $response_data = array();
		
		$response_data = $this->common_model->get_all_category()->result_array();
		//echo $this->db->last_query(); 
        $return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data);
        echo json_encode($return_data);
	}	 
    
	/* Product By Category */
	
	public function get_product_by_category()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();

        if (!isset($link) || trim($link) == "" || !isset($title) || trim($title) == ""  || !isset($id) || trim($id) == "") {
            echo $this->server . "get_product_by_category?category_id=&title=";
            echo "<br>Please pass parameters";
            exit;
        }
		
		if((isset($link) && trim($link) != "") && (isset($title) || trim($title) != "")  && (isset($id) || trim($id) != ""))
		{
			$expload_data = explode('&id=',$link);
			if(count($expload_data)>'1')
			{
				$content_id = $expload_data[1];
			}
			else
			{
				$content_id = '';
			}
			
			
			if(trim($content_id)!='')
			{
				$result = $this->common_model->get_product_by_cat_id($content_id);
				$total_result = $result->num_rows();
			}
			else
			{
				$slug = strtolower($title);
				$data = $this->common_model->get_content_by_slug($slug)->row_array();
				$result = $this->common_model->get_product_by_cat_id($data['id']);
				$total_result = $result->num_rows();
			}
			
			
			if($total_result=='0')
			{
				$message = "Sorry this category is not found in database.";	
			}
			else
			{
				$success = true;
				$message = "Product listed successfully.";	
				$response_data = $result->row_array();
				$response_data['introtext']=str_replace("\r\n",'', htmlspecialchars(str_replace('"',"'",$response_data['introtext'])));
				/* Product Images */
				$response_data['product_photos'] = $this->common_model->get_product_images($id)->row_array();
				if(!empty($response_data['product_photos']))
				{
					$response_data['product_photos'] =  $response_data['product_photos']['params'];
				}
				/* End */

				/* Product Price */
				$response_data['product_price'] = $this->common_model->get_product_price($id)->row_array();
				
				if(!empty($response_data['product_price']))
				{
					$response_data['product_price'] =  str_replace("\r\n",'', htmlspecialchars(str_replace('"',"'",$response_data['product_price']['content'])));
				}
				/* End */
			}
		}
		else
		{
			$success = true;
			$message = "You can not pass blank value";	
		}			
		$return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data);
		echo json_encode($return_data, JSON_UNESCAPED_SLASHES);
	}
	
	public function get_all_products()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "Listed All products";
        $response_data = array();
        
		$categories = $this->common_model->get_all_category()->result_array();
		$i=0;
		foreach($categories as $cat_val)
		{
			$link	=	$cat_val['link'];
			$id		=	$cat_val['id'];
			$title	=	$cat_val['title'];
			
			$expload_data = explode('&id=',$link);
			if(count($expload_data)>'1')
			{
				$content_id = $expload_data[1];
			}
			else
			{
				$content_id = '';
			}
			
			if(trim($content_id)!='')
			{
				$result = $this->common_model->get_product_by_cat_id($content_id);
				$total_result = $result->num_rows();
			}
			else
			{
				$slug = strtolower($title);
				$data = $this->common_model->get_content_by_slug($slug)->row_array();
				$result = $this->common_model->get_product_by_cat_id($data['id']);
				$total_result = $result->num_rows();
			}
			$response_data[] = $result->row_array();
			$response_data[$i]['introtext']=str_replace("\r\n",'', htmlspecialchars(str_replace('"',"'",$response_data[$i]['introtext'])));

			// Product Images
			$response_data[$i]['product_photos'] = $this->common_model->get_product_images($id)->row_array();
			
			if(!empty($response_data[$i]['product_photos']))
			{
				$response_data[$i]['type'] = 'image';				
				$product_photos =  json_decode($response_data[$i]['product_photos']['params'],true);
				$photos_set=array();
				for($j=1;$j<=20;$j++)
				{
					$image_var = 'img'.$j;
					if(!empty($product_photos[$image_var]) &&  trim($product_photos[$image_var])!="")
					{
						$temp=array();
						$temp['img']=GET_SLIDER_PATH.$product_photos[$image_var];
						array_push($photos_set,$temp);
					}	
				}
				$response_data[$i]['product_photos'] = $photos_set;
			}
			else
			{
				$response_data[$i]['type'] = 'video';
				$photos_set=array();
				$response = $this->common_model->get_product_video($id)->row_array();
	
				$iframe_code = trim($response['content']);
				
				if(trim($iframe_code)!='')
				{
					preg_match('/src="([^"]+)"/', $iframe_code, $match);
					$temp=array();
					$youtube_link = str_replace("//","",$match[1]);
					$temp['video']=$youtube_link;
					preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $youtube_link, $matches);
					$temp['img']="http://img.youtube.com/vi/".$matches[1]."/hqdefault.jpg";
					array_push($photos_set,$temp);
				}
				$response_data[$i]['product_photos'] = $photos_set;
			}
			
			//End

			// Product Price
			$product_price = array();
			$response_data[$i]['product_price'] = $this->common_model->get_product_price($id)->row_array();
			$response_data[$i]['product_title'] =  $title;
			if(!empty($response_data[$i]['product_price']))
			{
				$content = str_replace("\r\n",'', htmlspecialchars(str_replace('"',"'",$response_data[$i]['product_price']['content']))).'<br>';
				preg_match_all('/\{([^}]*)\}/', $content, $matches);
				$array = array_unique($matches[1]);
				
				foreach($array as $val)
				{
					$export = explode('redshop_price',$val);
					if(count($export)=='2')
					{
						$comma_ex = explode(',',$export[1]);
						$product_data = $comma_ex[0];
						$product_val = explode(':',$product_data);
						//echo print_r($product_val);
						$product_id = $product_val[1];
						$section_id = $comma_ex[1];
						$product_price[] = $this->common_model->price($section_id)->row_array();
						
					}
					if(isset($product_id))
					{
						$response_data[$i]['product_id']= $product_id;
					}
				}
				$response_data[$i]['price'] = $product_price;
			}
			else
			{
				unset($response_data[$i]);
				//$response_data[$i]['price'] = $product_price;	
			}
			// End 
			unset($response_data[$i]['product_price']);	
			$i++;
			$photos_set='';
			$product_photos='';
			$content ='';
			$array = '';
			$product_price='';
		}
		if(count($response_data)>0)
		{
			$success=true;
			$message="Success.";
		}
		else
		{
			$message="No records found.";
		}
		
		$return_data = array("success" => $success, "message" => $message, 'response_data' => array_values($response_data));
		echo json_encode($return_data, JSON_UNESCAPED_SLASHES);
	}
	
	
	/* Product Add to cart */
	
	public function add_to_cart()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();
	
        if(!isset($session_id) || trim($session_id) == "" || !isset($user_id) || trim($user_id) == "" || !isset($product_id) || trim($product_id) == "" || !isset($subattribute_id) || trim($subattribute_id) == "" || !isset($subattribute_color_id) || trim($subattribute_color_id) == ""  || !isset($qty) || trim($qty) == "") 
        {
            echo $this->server . "add_to_cart?session_id=&user_id=&product_id=&subattribute_id=&subattribute_color_id=&qty";
            echo "<br>Please pass parameters";
            exit;
        }

		$check_session = $this->common_model->get_time_by_session_id($session_id)->row_array();
		if(!empty($check_session))
		{
			$success = true;
			$usercart_time = $this->common_model->check_usercart($user_id)->row_array();
			
			if(!empty($usercart_time))
			{
				$user_mdate = $usercart_time['mdate'];
				$cart_id = $usercart_time['cart_id'];
			}
			else
			{
				$insert_ussercart_data['user_id'] = $user_id;
				$insert_ussercart_data['cdate'] = time();
				$insert_ussercart_data['mdate'] = time();
				$this->common_model->insert("m52rw_redshop_usercart",$insert_ussercart_data);		
				$cart_id = $this->db->insert_id();
				$usercart_data = $this->common_model->get_usercart_byid($cart_id)->row_array();
				$user_mdate = $usercart_data['mdate'];
				
			}
			$session_date = $check_session['time']; 

			if($session_date!=$user_mdate)
			{
				$where["cart_id"] = $cart_id;
				$this->common_model->delete("m52rw_redshop_usercart_item",$where);
				$upd['mdate'] = $session_date;
				$where['user_id'] = $user_id;
				$this->common_model->update("m52rw_redshop_usercart",$upd,$where);
			}	

			/* Check Product In Cart */
			$check_usercart = $this->common_model->check_cart($session_id,$product_id)->num_rows();
			if($check_usercart=='0')
			{
				/* Insert Data IN Cart */
				$cart_data['session_id ']	=	$session_id;
				$cart_data['product_id']	=	$product_id;
				$cart_data['section']		=	'product';	
				$cart_data['qty']			=	$qty;
				$cart_data['time']			=	time();
				
				$this->common_model->insert("m52rw_redshop_cart",$cart_data);
				/* End */

				$usercart_res = $this->common_model->get_usercart_item($cart_id);
				$usercart_count = $usercart_res->num_rows();
				$usercart_item = $usercart_res->result_array();
				
				$cart_data1['cart_id']= $cart_id;
				$cart_data1['product_id']= $product_id;
				if($usercart_count==0)
				{
					$cart_data1['cart_idx']= '0';
				}	
				else
				{
					$cart_data1['cart_idx']= $usercart_count;	
				}	
				$cart_data1['cart_id']= $cart_id;
				$cart_data1['product_quantity']= $qty;
				
				$insert_cart = $this->common_model->insert("m52rw_redshop_usercart_item",$cart_data1);
				$cart_item_id1 = $this->db->insert_id();

				/* Data for product Attributes */
				$produt_attribute = $this->common_model->get_product_attribute($product_id)->row_array();
				$attribute_id = $produt_attribute['attribute_id'];
				if(isset($attribute_id) && trim($attribute_id)!='')
				{
					$att_data['parent_section_id'] = $product_id;
					$att_data['section'] = 'attribute';
					$att_data['section_id'] = $attribute_id;
					$att_data['cart_item_id'] = $cart_item_id1;
					$this->common_model->insert("m52rw_redshop_usercart_attribute_item",$att_data);

					/* Property By Attribute */
					$attribute_prop = $this->common_model->get_property_by_attribute($attribute_id)->row_array();
					$property_id = $attribute_prop['property_id'];
					
					$insert_prop['parent_section_id'] = $attribute_id;
					$insert_prop['section'] = 'property';
					$insert_prop['section_id'] = $property_id;
					$insert_prop['cart_item_id'] = $cart_item_id1;
					$this->common_model->insert("m52rw_redshop_usercart_attribute_item",$insert_prop);
				}
				/* End */
				
				/* Data for product subproperty */
				$subproperty_data['parent_section_id'] = $subattribute_color_id;
				$subproperty_data['section'] = 'subproperty';
				$subproperty_data['section_id'] = $subattribute_id;
				$subproperty_data['cart_item_id'] = $cart_item_id1;
				$final_res = $this->common_model->insert("m52rw_redshop_usercart_attribute_item",$subproperty_data);
				$final_id = $this->db->insert_id();
				/* End */
				if($final_res)
				{
					$success = true;
					$message = "Product Successfully added in cart";
					$response_data = $this->common_model->get_add_cart_list($subattribute_color_id)->row_array();
				}
				else
				{
					$success = true;
					$message = "Something Went wrong. Please try again";
				}
			}
			else
			{
				$success = true;
				$message = "Product already added in cart.";
			}	
		}
		else
		{
			$success = true;
			$message = "Your session time is expaired. Please login first.";
		}			
		$return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data);
		echo json_encode($return_data, JSON_UNESCAPED_SLASHES);
	}
	
	// Delect item from cart
	public function delete_cart_item()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();
	
        if(!isset($cart_item_id) || trim($cart_item_id) == "" || !isset($session_id) || trim($session_id) == "" || !isset($product_id) || trim($product_id) == "") 
        {
            echo $this->server . "delete_cart_item?cart_item_id=";
            echo "<br>Please pass parameters";
            exit;
        }
        
        $check_cart_data = $this->common_model->add_cart_item($cart_item_id)->num_rows();
        
        if($check_cart_data!='0')
        {
			/* Delete cart Product */
			$cartWhere['session_id'] =  $session_id;
			$cartWhere['product_id'] =  $product_id;
			$this->common_model->delete("m52rw_redshop_cart",$cartWhere); 
			/* End */
			
			$where['cart_item_id'] = $cart_item_id;
			/* Delete Product Item */
			$this->common_model->delete("m52rw_redshop_usercart_item",$where);
			/* Delete Product Property */
			$this->common_model->delete("m52rw_redshop_usercart_attribute_item",$where);

			$success = true;
			$message = "Product successfully deleted.";
		}
		else
		{
			$success = true;
			$message = "This item not found.";
		}	
		$return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data);
		echo json_encode($return_data, JSON_UNESCAPED_SLASHES);
	}
	
	public function add_cart_list()
	{
		
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();
        $sum_total = "";
		
        if(!isset($session_id) || trim($session_id) == "" || !isset($user_id) || trim($user_id) == "") 
        {
            echo $this->server . "add_cart_list?session_id=&user_id=";
            echo "<br>Please pass parameters";exit;
        }
        
		$usercart_result = $this->common_model->check_usercart($user_id);
		$total_data = $usercart_result->num_rows();
        if($total_data!='0')
        {
			$check_usercart = $usercart_result->row_array();
			$cart_id = $check_usercart['cart_id'];
			$product_items = $this->common_model->get_usercart_item($cart_id)->result_array();
			if(!empty($product_items))
			{
				foreach($product_items as $product)
				{
					$item_id[] = $product['cart_item_id'];	
				}
				$cart_item_id = implode(',',$item_id);
				
				$success = true;
				$message = "Your Cart List.";
				$response_data = $this->common_model->cart_list_by_user($cart_item_id)->result_array();
				$sum = '0';
				foreach($response_data as $value)
				{
					$sum = $sum + $value['subattribute_color_price'];
				}
				
				$sum_total["sub_total"] = $sum;
				
			}
			else
			{
				$success = true;
				$message = "Your Cart is empty.";
			}
		}
		else
		{
			$success = true;
			$message = "Your Cart is empty.";
		}
		$return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data, 'response_sub_total' => $sum_total);
		echo json_encode($return_data, JSON_UNESCAPED_SLASHES);
	}
	
	// Clear cart Details
	
	public function clear_cart()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();
		
        if(!isset($session_id) || trim($session_id) == "" || !isset($user_id) || trim($user_id) == "") 
        {
            echo $this->server . "add_cart_list?session_id=&user_id=";
            echo "<br>Please pass parameters";exit;
        }
        
        $check_usercart = $this->common_model->check_usercart($user_id)->row_array();
        $cart_id = $check_usercart['cart_id'];
		$product_items = $this->common_model->get_usercart_item($cart_id)->result_array();
		if(!empty($product_items))
		{
			foreach($product_items as $product)
			{
				$item_id[] = $product['cart_item_id'];	
			}
			$cart_item_id = implode(',',$item_id);
			$where['session_id'] = $session_id;
			
			$this->common_model->delete('m52rw_redshop_cart',$where);
			$this->common_model->delete_in('m52rw_redshop_usercart_item',$cart_item_id);
			$this->common_model->delete_in('m52rw_redshop_usercart_attribute_item',$cart_item_id);
			
			$success = true;
			$message = "Cart successfully Cleared.";
		}
		else
		{
			$success = true;
			$message = "Your Cart is empty.";
		}
		$return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data);
		echo json_encode($return_data);
	}
	
	// Check Out 
	public function checkout()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();
		
        if(!isset($session_id) || trim($session_id) == "" || !isset($user_id) || trim($user_id) == "" || !isset($listing_address) || trim($listing_address) == "" || !isset($city) || trim($city) == "" || !isset($state_code) || trim($state_code) == "" || !isset($zipcode) || trim($zipcode) == "") 
        {
            echo $this->server."checkout?session_id=&user_id=&listing_address=&city=&state_code=&sub_total=";
            echo "<br>Please pass parameters";exit;
        }
		
		$user_info = $this->common_model->get_userinfo_data($user_id)->row_array();
		/* New Shipping Entry */
		$users_info_data['user_id']=$user_id;
		$users_info_data['user_email'] = $user_info['user_email'];
		$users_info_data['shopper_group_id'] = $user_info['shopper_group_id'];
		$users_info_data['address_type']='ST';
		$users_info_data['country_code']='USA';
		$users_info_data['state_code']=$state_code;
		$users_info_data['address']=$listing_address;
		$users_info_data['zipcode']=$zipcode;
		$users_info_data['city']=$city;

		$res = $this->common_model->insert('m52rw_redshop_users_info',$users_info_data);
		$new_user_id = $this->db->insert_id();	
		/* End */
		if($res)
		{
			$success=true;
			$message="Check out success.";
			$response_data = $this->common_model->get_new_userinfo_data($new_user_id)->row_array();
		}
		else
		{
			$success=true;
			$message="Something Went wrong. Please try again.";
		}
		$return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data);
		echo json_encode($return_data);
	}
	
	
	/* Get Order With Shipping Address */
	
	public function get_order_details()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();
		
        if(!isset($session_id) || trim($session_id) == "" || !isset($user_id) || trim($user_id) == "" || !isset($users_info_id) || trim($users_info_id) == "") 
        {
            echo $this->server."get_order_details?session_id=&user_id=&users_info_id=";
            echo "<br>Please pass parameters";exit;
        }
		$result_set['user_details'] = $this->common_model->get_new_userinfo_data($users_info_id)->row_array();
		
		$usercart_result = $this->common_model->check_usercart($user_id);
		$check_usercart = $usercart_result->row_array();
		$cart_id = $check_usercart['cart_id'];
		$product_items = $this->common_model->get_usercart_item($cart_id)->result_array();

		foreach($product_items as $product)
		{
			$item_id[] = $product['cart_item_id'];	
		}
		$cart_item_id = implode(',',$item_id);
		
		$success = true;
		$message = "Your Order Details.";
		$response_data = $this->common_model->cart_list_by_user($cart_item_id)->result_array();
		$sum = '0';
		foreach($response_data as $value)
		{
			$sum = $sum + $value['subattribute_color_price'];
		}
		$result_set['cart_details']=$response_data;
		
		$result_set["sub_total"] = $sum;
		
		//echo "<pre>";print_r($result_set);die;
		
		$return_data = array("success" => $success, "message" => $message, 'response_data' => $result_set);
		echo json_encode($return_data);
	}
	
	
	/* Order Confirmation */
	
	public function order_confirmation()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();
		
        if(!isset($session_id) || trim($session_id) == "" || !isset($user_id) || trim($user_id) == "" || !isset($users_info_id) || trim($users_info_id) == "") 
        {
            echo $this->server."order_confirmation?session_id=&user_id=&users_info_id=";
            echo "<br>Please pass parameters";exit;
        }
        

		$check_usercart = $this->common_model->check_usercart($user_id)->row_array();
		$cart_id = $check_usercart['cart_id'];
		$product_items = $this->common_model->get_usercart_item($cart_id)->result_array();
		
		foreach($product_items as $product)
		{
			$item_id[] = $product['cart_item_id'];	
		}
		$cart_item_id = implode(',',$item_id);
		
		
		$response_data = $this->common_model->cart_list_by_user($cart_item_id)->result_array();
		$sum_total = '0';
		foreach($response_data as $value)
		{
			$sum_total = $sum_total + $value['subattribute_color_price'];
		}
		
        if(isset($coupon_data) && trim($coupon_data)!='')
        {
			$coupon_vals = json_decode($coupon_data);	
			$coupon_id =  $coupon_vals->coupon_id;
			$get_coupon_code =  $coupon_vals->coupon_code;
			$percent_or_total =  $coupon_vals->percent_or_total; // 0 for % and 1 for amount
			$coupon_value =  $coupon_vals->coupon_value;
			$coupon_type =  $coupon_vals->coupon_type; //0 For global and 1 for special user
			$coupon_left =  $coupon_vals->coupon_left;
			
			if($percent_or_total=='0')
			{
				$coupon_discount = (($sub_total*$coupon_value)/100);
			}
			else
			{
				$coupon_discount = 	$coupon_value;
			}
		}
		else
		{
			$coupon_discount = 	'0.00';
		}
        
		$user_info = $this->common_model->get_userinfo_data($user_id)->row_array();
		
		/* Order Data Start */
		
		if(isset($exlife) && trim($exlife)!='')
		{
			$order_data['exlife'] = 'Extended Hosting';
			$order_data['exp_benifit_price'] = '7';
			$order_data['exp_benefit'] = '1';
		}
		
		$order_data['user_id']=$user_id;
		$order_data['user_info_id'] = $user_info['users_info_id'];
		$order_data['order_subtotal'] = $sub_total;
		$order_data['order_tax']='0.00';
		$order_data['order_shipping']='0.00';
		$order_data['order_shipping_tax']='0.00';
		$order_data['coupon_discount']=$coupon_discount;
		$order_data['order_discount']='0.00';
		$order_data['special_discount_amount']='0.00';
		$order_data['payment_dicount']='0.00';
		$order_data['order_status']='P';
		$order_data['order_payment_status']='Unpaid';
		$order_data['cdate']=time();
		$order_data['mdate']=time();
		$order_data['ship_method_id'] = '';
		$order_data['ip_address']= $_SERVER['REMOTE_ADDR'];
		
		$tax_shipping = $order_data['order_tax'] + $order_data['order_shipping'] + $order_data['order_shipping_tax'];
		if(isset($order_data['exp_benifit_price']) && trim($order_data['exp_benifit_price'])!='') 
		{
			$tax_shipping =$tax_shipping + $order_data['exp_benifit_price'];
		}
		$discount = $order_data['coupon_discount'] + $order_data['order_discount'] + $order_data['special_discount_amount'] + $order_data['payment_dicount'];
		$order_total = (($sum_total + $tax_shipping) - $discount);
		$order_data['order_total']=	$order_total;
		
		// Insert Order data
		$this->common_model->insert('m52rw_redshop_orders',$order_data);
		$order_id = $this->db->insert_id();		
		
		// Update Order number
		$update['order_number'] = 'epd'.$order_id;
		$where['order_id'] = $order_id;
		$this->common_model->update('m52rw_redshop_orders',$update,$where);
		/* End */
		
		/* Main User Entry */	
		$user_order_data["users_info_id"]=$user_info['users_info_id'];
		$user_order_data["order_id"]=$order_id;
		$user_order_data["user_id"]=$user_id;
		$user_order_data["firstname"]=$user_info['firstname'];
		$user_order_data["lastname"]=$user_info['lastname'];
		$user_order_data["address_type"]=$user_info['address_type'];
		$user_order_data["shopper_group_id"]=$user_info['shopper_group_id'];
		$user_order_data["address"]=$user_info['address'];
		$user_order_data["city"]=$user_info['city'];
		$user_order_data["country_code"]=$user_info['country_code'];
		$user_order_data["state_code"]=$user_info['state_code'];
		$user_order_data["zipcode"]=$user_info['zipcode'];
		$user_order_data["phone"]=$user_info['phone'];
		$user_order_data["user_email"]=$user_info['user_email'];
		
		$this->common_model->insert('m52rw_redshop_order_users_info',$user_order_data);
		
		/* Shipping user enrty */
		
		$user_shipping = $this->common_model->get_new_userinfo_data($users_info_id)->row_array();
		
		$user_ship_data["users_info_id"]=$user_shipping['users_info_id'];
		$user_ship_data["order_id"]=$order_id;
		$user_ship_data["user_id"]=$users_info_id;
		$user_ship_data["firstname"]=$user_shipping['firstname'];
		$user_ship_data["lastname"]=$user_shipping['lastname'];
		$user_ship_data["address_type"]=$user_shipping['address_type'];
		$user_ship_data["shopper_group_id"]=$user_shipping['shopper_group_id'];
		$user_ship_data["address"]=$user_shipping['address'];
		$user_ship_data["city"]=$user_shipping['city'];
		$user_ship_data["country_code"]=$user_shipping['country_code'];
		$user_ship_data["state_code"]=$user_shipping['state_code'];
		$user_ship_data["zipcode"]=$user_shipping['zipcode'];
		$user_ship_data["phone"]=$user_shipping['phone'];
		$user_ship_data["user_email"]=$user_shipping['user_email'];
		
		$this->common_model->insert('m52rw_redshop_order_users_info',$user_ship_data);
		/* End */
		
		/* Order status Entry */
		$order_status['order_id'] = $order_id;
		$order_status['order_status']='P';
		$order_status['order_payment_status'] = '';
		$order_status['date_changed']=time();
		$order_status['customer_note']='';

		$this->common_model->insert('m52rw_redshop_order_status_log',$order_status);
		
		/* End */
		
		
		/* Order Items Entry */
		$order_list = $this->common_model->cart_list_by_user($cart_item_id)->result_array();
		
		foreach($order_list as $order_item)
		{
			$order_items_data['order_id']=$order_id;
			$order_items_data['user_info_id']=$user_info['users_info_id'];
			$order_items_data['supplier_id']='0';
			$order_items_data['product_id']=$order_item['product_id'];
			$order_items_data['order_item_sku']=$order_item['subattribute_color_name'];
			$order_items_data['order_item_name']=$order_item['subattribute_color_name'];
			$order_items_data['product_quantity']=$order_item['subattribute_color_name'];
			$order_items_data['product_item_price']=$order_item['subattribute_color_name'];
			$order_items_data['product_item_price_excl_vat']=$order_item['subattribute_color_name'];
			$order_items_data['product_final_price']=$order_item['subattribute_color_name'];
			$order_items_data['order_item_currency'] = '$';
			$order_items_data['order_status']='P';
			$order_items_data['cdate']=time();
			$order_items_data['mdate']=time();
			$order_items_data['product_item_old_price']=$order_item['subattribute_color_name'];
			
			$this->common_model->insert('m52rw_redshop_order_item',$order_items_data);
		}
		
		/*$to = $agent_email;
		$subject = "IMOTO Order # 'epd'".$order_id" has been received - NOT SCHEDULED";

		$message = "<html>
						<head>
							<title>Order Confirm</title>
						</head>
						<body>
							<p>Thank you so much for your order! <b style='color:red;'>Please note that your order is NOT scheduled</b>, we will be in touch shortly to finalize the date and time of your order.</p>
							<p>Your order is: # 'epd'".$order_id"</p>
							<p>Our office hours are 8:30-5 CST. All orders placed during off-hours will be addressed the next business morning.</p>
							<p>In the meantime, please take a look at our <a href='http://www.imotophoto.com/images/tutorials/Pre-Shoot_Checklist.pdf'>Preparing Your IMOTO Listing page</a>.</p>
							<p>Also, did you know that IMOTO is on Facebook? <a href='http://www.Facebook.com/imotosocial'>www.Facebook.com/imotosocial</a></p>
							<p>We highly encourage you to be present for all photo shoots. In the event that you are not there, IMOTO photographers will only take the exact number of photos that you ordered. </p>
							<p style='color:red;font-weight:bold;'>Please review the photos once the photographer has completed the shoot. It is IMOTO policy that you have deemed all photos approved after you have reviewed the photos on site. You will not be able to choose the photos after we leave the property. </p>
							<p>If additional photos are requested we will charge you the standard fee to return to the property.</p>
							<p>The photos will be delivered by the next business morning.</p>
						</body>
					</html>";

		$header = 'From:' . ADMINEMAIL . "\r\n" .
		'Reply-To: ' . ADMINEMAIL . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		//$retval = mail ($to,$subject,$message,$header);
		$retval = send_mail($to, $subject, $message);

		if ($retval == true) 
		{
			$success = true;
			$message = "An e-mail has been sent to your e-mail address with your password.";
		} 
		else 
		{
			$message = "An error occurred.Please try again.";
		}

		*/
		
		$success = true;
		$message = "You have successfully confirm your order.";
		
      
		$where_cart['session_id'] = $session_id;
		$cart_where['cart_id']=$cart_id;
		
		$this->common_model->delete('m52rw_redshop_cart',$where_cart);
		$this->common_model->delete('m52rw_redshop_usercart',$cart_where);
		$this->common_model->delete_in('m52rw_redshop_usercart_item',$cart_item_id);
		$this->common_model->delete_in('m52rw_redshop_usercart_attribute_item',$cart_item_id);
		/* END */
		
        $return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data);
		echo json_encode($return_data);
	}
	
	/* Validate Coupon */
	public function validate_coupon()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();

        if(!isset($session_id) || trim($session_id) == "" || !isset($user_id) || trim($user_id) == "" || !isset($coupon_code) || trim($coupon_code) == "") 
        {
            echo $this->server."validate_coupon?session_id=&user_id=&coupon_code=";
            echo "<br>Please pass parameters";exit;
        }
		$response_data1 = array();
		
		$check_date = time();
		$valid_coupon = $this->common_model->get_validate_coupon($coupon_code,$check_date);
		//echo $this->db->last_query();
		$coupon_count = $valid_coupon->num_rows();
		if($coupon_count!='0')
		{
			$response_data = $valid_coupon->row_array();
			if($response_data['userid']!='0')
			{
				if($response_data['userid']==$user_id)
				{
					$success = true;
					$message = "You are a special user to use Coupon.";
				}
				else
				{
					$success = true;
					$message = "This coupon is for only special users. Please try again.";
				}	
			}
		}
		else
		{
			$success = true;
			$message = "Your entered Coupon is invalid or out of date.";
		}

		$return_data = array("success" => $success, "message" => $message, 'response_data' => $response_data);
		echo json_encode($return_data);
	}
	
	/* Get User Order History */
	
	function get_user_order_history()
	{
        $request_data = $_REQUEST;
        extract($request_data, EXTR_SKIP);
        $success = false;
        $message = "";
        $response_data = array();

        if(!isset($user_id) || trim($user_id) == "") 
        {
            echo $this->server."get_user_order_history?user_id=";
            echo "<br>Please pass parameters";exit;
        }
        
        $result = $this->common_model->get_order_history($user_id);
        $count_result = $result->num_rows();
        if($count_result!='0')
        {
			$success=true;
			$message="Order history in your account.";	
			$response_data=$result->result_array();
			$i=0;
			$order_item_name="";
			$result=array();
			foreach($response_data as $response_val)
			{
				$temp=array();
				$temp=$response_val;
				$order_id = $response_val['order_id'];
				$order_items = $this->common_model->get_order_items($order_id)->result_array();
				foreach($order_items as $values)
				{
					$order_item_name[]=$values['order_item_name'];
				}
				
				$temp['order_item_name']=implode(",",$order_item_name);
				array_push($result,$temp);
				$i++;
			}
			
		}
		else
		{
			$success=true;
			$message="There are no order history in your account";	
		}
		$return_data = array("success" => $success, "message" => $message, 'response_data' => $result);
		echo json_encode($return_data);
	}
}
